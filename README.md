Welcome to DTU course 02148 (Introduction to Coordination in Distributed Applications).

Here you will find:
 * [Official course description](http://kurser.dtu.dk/course/02148)
 * [Public course information](http://www2.imm.dtu.dk/courses/02148/)
 * [Lecture plan](schedule.md)
 * [Exercises](exercises.md)