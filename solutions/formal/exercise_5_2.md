We can follow the approach described in the solution to exercise 5.1. However, there are two main disadvantages. First, we use one global lock, which is not always necessary, as not all transitions are competing with each other. The second disadvantage is the implicit “busy wait” in the process of checking if there are sufficient tokens in the input places. 

Here is a variant of the previous approach that mitigates some of these problems. The main idea is to use more fine-grained locks, in particular one for each set of competing transitions.

We consider two transitions as competing if they are related by `%`, the least equivalence relation (i.e. transitive, reflective and symmetric relation) such that if a place is in the input of transitions `t` and `t’` then `t % t`. Intuitively if `t1` and `t2` are competing the acquisition of tokens by one of them can affect the enabledness of the other one. 

We will then create a lock `“lock C”` for each equivalence class `C` in `#`, whose size is greater than 1. 

Note that equivalence classes whose size is 1 correspond to transitions that don not compete with any other transition. Such transitions can be simply implemented as follows

```
while(true)
    // Get the input tokens
    p0.get(“token”);
    ...
    pn.get(“token);
    // Execute transition t
    ...
    // Generate the output tokens
    q0.put(“token”);
    ...
    qm.put(“token”);
end
```

The rest of the transitions t with competing class C can be implemented as follows 

```
while(true)
	// acquire lock
	lock.get(“lock C”);
	// check if there are tokens in all input places
    if p0.queryp(“token) & .. & pn.queryp(“token”) then
	    // Get the input tokens
        p0.get(“token”);
        ...
        pn.get(“token);
        // Execute transition t
        print(“t”);
        // Generate the output tokens
        q0.put(“token”);
        ...
        qm.put(“token”);
    end
    // restore lock
    put(“lock C”);
end
```
