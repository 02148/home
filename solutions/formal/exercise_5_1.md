A possible solution to this exercise could be as follows. First, we introduce a global lock space `lock` with a tuple `"lock"` on it.  Each task `t` with input places `p0...pn` and output places `q0...qm` is implemented as

```
while(true)
	// acquire lock
	lock.get(“lock”);
	// check if there are tokens in all input places
    if p0.queryp(“token”) & .. & pn.queryp(“token”) then
	    // Get the input tokens
        p0.get(“token”);
        ...
        pn.get(“token);
        // Execute transition t
        print(“t”);
        // Generate the output tokens
        q0.put(“token”);
        ...
        qm.put(“token”);
    end
    // restore lock
    put(“lock”);
end
```

In the concrete example of the exercise and assuming that the token names are p0, p1, p2 (in occidental reading order), we obtain the following implementations:

```
// Implementation of A
while(true)
	// acquire lock
	lock.get(“lock”);
	// check if there are tokens in all input places
    if p0.queryp(“token”) then
	    // Get the input tokens
        p0.get(“token”);
        // Execute transition A
        print(“A”);
        // Generate the output tokens
        p0.put(“token”);
    end
    // restore lock
    put(“lock”);
end

// Implementation of B
while(true)
	// acquire lock
	lock.get(“lock”);
	// check if there are tokens in all input places
    if p0.queryp() p1.queryp() then
	    // Get the input tokens
        p0.get(“token”);
        p1.get(“token);
        // Execute transition B
        print(“B”);
        // Generate the output tokens
        p2.put(“token”);
    end
    // restore lock
    put(“lock”);
end

// Implementation of C
while(true)
	// acquire lock
	lock.get(“lock”);
	// check if there are tokens in all input places
    if p1.queryp() then
	    // Get the input tokens
        p1.get(“token”);
        // Execute transition C
        print(“C”);
        // Generate the output tokens
        p1.put(“token”);
    end
    // restore lock
    put(“lock”);
end

// Implementation of D
while(true)
	// acquire lock
	lock.get(“lock”);
	// check if there are tokens in all input places
    if p2.queryp() then
	    // Get the input tokens
        p2.get(“token”);
        // Execute transition D
        print(“D”);
        // Generate the output tokens
        p0.put(“token”);
        p1.put(“token”);
    end
    // restore lock
    put(“lock”);
end
```