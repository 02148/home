We consider there the basic solution to exercise 5.1.

Read arcs could then be integrated in the solution by not removing the tokens they read from. Assuming a predicate `read(p,t)` determines if `p` is read-only conndition for `t` we can do as follows


```
        ...
	    // Get the input tokens
        if !read(p0,t) p0.get(“token”);
        ...
        if !read(pn,t) pn.get(“token);
        ...
end
```

Inhibitor arcs could be integrated by negating the condition when we do the enabledness check. For example if `p0` is an ordinary input place and `p1' we could change the enabledness checks as follows: 


```
    ...
	// check if there are tokens in all input places
    if p0.queryp(“token”) & !p1.queryp(“token”) then
    ...
end
```

Reset arcs require that, once the conditions for firing the transition are met, we make sure that all tokens in the reset place are removed. This could be done as follows:

```
	...
	// Reset place p
	while (p.getp("token")) do skip
	...
    
end
```