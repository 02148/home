# Protocol Projection Example

**Version with highlighted steps:** https://dtu.bogoe.eu/02148/projection/

## The Protocol
```
1: Alice.("forward", to) -> Bob.tupleName("forward", string);
2: if tupleName(to) == "Charlie"@Bob then
3:    Bob."hallo" -> Charlie.notUsed("hallo")
4: else
5:    Bob."hey" -> Dave._("hey")
```
where `to` is `"Charlie"` or `"Dave"`.

For illustrative purposes, we will defined the following variables:

- `HalloProtocol` is the whole protocol (line 1-5).
- `Line2To5` is line 2-5.
- `Line1` is line 1.
- `Line3` is line 3.
- `Line5` is line 5.

In the following sections, the `project` function will be *calculated* for each actor based on the definition [here](https://gitlab.gbar.dtu.dk/02148/home/blob/master/protocols.md#48-distributed-implementation-of-a-protocol).


## Projection for Alice
```
project(HalloProtocol, Alice)
```
```
project(Line1 ; Line2To5, Alice)
```
```
project(Line1, Alice) ;
project(Line2To5, Alice)
```
```
project(Alice.("forward", to) -> Bob.tupleName("forward", string), Alice) ;
project(Line2To5, Alice)
```
```
channel(Alice, Bob).put(("forward", to)) ;
project(Line2To5, Alice)
```
```
channel(Alice, Bob).put(("forward", to)) ;
project(if tupleName(to) == "Charlie"@Bob then Line3 else Line5, Alice)
```
```
channel(Alice, Bob).put(("forward", to)) ;
{}
```
```
channel(Alice, Bob).put(("forward", to))
```


## Projection for Bob
```
project(HalloProtocol, Bob)
```
```
project(Line1 ; Line2To5, Bob)
```
```
project(Line1, Bob) ;
project(Line2To5, Bob)
```
```
project(Alice.("forward", to) -> Bob.tupleName("forward", string), Bob) ;
project(Line2To5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
project(Line2To5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
project(if tupleName(to) == "Charlie"@Bob then Line3 else Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    for all q in (Line3 or Line5) do channel(Bob, q).put("then") ;
    project(Line3, Bob)
else
    for all q in (Line3 or Line5) do channel(Bob, q).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    for all q in {Charlie, Dave} do channel(Bob, q).put("then") ;
    project(Line3, Bob)
else
    for all q in {Charlie, Dave} do channel(Bob, q).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    project(Line3, Bob)
else
    for all q in {Charlie, Dave} do channel(Bob, q).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    project(Line3, Bob)
else
    channel(Bob, Charlie).put("else") ;
    channel(Bob, Dave).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    project(Bob."hallo" -> Charlie.notUsed("hallo"), Bob)
else
    channel(Bob, Charlie).put("else") ;
    channel(Bob, Dave).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    channel(Bob, Charlie).put("hallo")
else
    channel(Bob, Charlie).put("else") ;
    channel(Bob, Dave).put("else") ;
    project(Line5, Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    channel(Bob, Charlie).put("hallo")
else
    channel(Bob, Charlie).put("else") ;
    channel(Bob, Dave).put("else") ;
    project(Bob."hey" -> Dave._("hey"), Bob)
```
```
tupleName := channel(Alice, Bob).get("forward", string) ;
if tupleName(to) == "Charlie" then
    channel(Bob, Charlie).put("then") ;
    channel(Bob, Dave).put("then") ;
    channel(Bob, Charlie).put("hallo")
else
    channel(Bob, Charlie).put("else") ;
    channel(Bob, Dave).put("else") ;
    channel(Bob, Dave).put("hey")
```


## Projection for Charlie
```
project(HalloProtocol, Charlie)
```
```
project(Line1 ; Line2To5, Charlie)
```
```
project(Line1, Charlie) ;
project(Line2To5, Charlie)
```
```
project(Alice.("forward", to) -> Bob.tupleName("forward", string), Charlie) ;
project(Line2To5, Charlie)
```
```
{} ; 
project(Line2To5, Charlie)
```
```
project(Line2To5, Charlie)
```
```
project(if tupleName(to) == "Charlie"@Bob then Line3 else Line5, Charlie)
```
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	project(Line3, Charlie)
else
	project(Line5, Charlie)
```
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	project(Bob."hallo" -> Charlie.notUsed("hallo"), Charlie)
else
	project(Line5, Charlie)
```
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	notUsed := channel(Bob, Charlie).get("hallo")
else
	project(Line5, Charlie)
```
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	notUsed := channel(Bob, Charlie).get("hallo")
else
	project(Bob."hey" -> Dave._("hey"), Charlie)
```
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	notUsed := channel(Bob, Charlie).get("hallo")
else
	{}
```
**OPTIONAL SIMPLIFICATION:**
```
branch := channel(Bob, Charlie).get(string) ;
if branch == "then" then
	notUsed := channel(Bob, Charlie).get("hallo")
```


## Projection for Dave
```
project(HalloProtocol, Dave)
```
```
project(Line1 ; Line2To5, Dave)
```
```
project(Line1, Dave) ;
project(Line2To5, Dave)
```
```
project(Alice.("forward", to) -> Bob.tupleName("forward", string), Dave) ;
project(Line2To5, Dave)
```
```
{} ; 
project(Line2To5, Dave)
```
```
project(Line2To5, Dave)
```
```
project(if tupleName(to) == "Charlie"@Bob then Line3 else Line5, Dave)
```
```
branch := channel(Bob, Dave).get(string) ;
if branch == "then" then
	project(Line3, Dave)
else
	project(Line5, Dave)
```
```
branch := channel(Bob, Dave).get(string) ;
if branch == "then" then
	project(Bob."hallo" -> Charlie.notUsed("hallo"), Dave)
else
	project(Line5, Dave)
```
```
branch := channel(Bob, Dave).get(string) ;
if branch == "then" then
	{}
else
	project(Line5, Dave)
```
```
branch := channel(Bob, Dave).get(string) ;
if branch == "then" then
	{}
else
	project(Bob."hey" -> Dave._("hey"), Dave)
```
```
branch := channel(Bob, Dave).get(string) ;
if branch == "then" then
	{}
else
	channel(Bob, Dave).get("hey")
```
**OPTIONAL SIMPLIFICATION:**
```
branch := channel(Bob, Dave).get(string) ;
if branch == "else" then
	channel(Bob, Dave).get("hey")
```