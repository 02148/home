// Thread 1 is the butler
// Threads 2-4 are the philosophers
// Forks are represented with 0, 1 and 2.
// This solutions is based on the use of tickets
// Tickets are represented by -1

par 
   board.put(0);
   board.put(1);
   board.put(2);
   board.put(-1);
   board.put(-1)
[]
   loop true -> board.get(-1);
		board.get(1);
                board.get(0);
                board.put(0);
                board.put(1);
		board.put(-1)
   pool
[]
   loop true -> board.get(-1);
		board.get(1);
                board.get(2);
                board.put(1);
                board.put(2);
		board.put(-1)
   pool
[]
   loop true -> board.get(-1);
		board.get(2);
                board.get(0);
                board.put(2);
                board.put(0);
		board.put(-1)
   pool
rap