# Exercises for 02148

## 1. Programming with spaces

### Exercise 1.1. Installing jSpace

Install [jSpace](https://github.com/pSpaces/jSpace) following the instructions in the repository.

If the installation instructions don't work for you, we have prepared a zip in FileSharing with the latest version of jSpace and its dependencies. You can use it to create an Eclipse project as follows:

1. Import the zip archive (jSpace-Project.zip) into Eclipse
2. In Eclipse press File -> Import from the top menu
3. Expand the “General” tab, select ”Existing Projects into Workspace” and press “next”
4. Press “Select archive file” and locate the zip archive, jSpace-Project.zip
5. Press “Finish”

To check that it works, try to run the file “App.java” in the package “common.src.main”

### Exercise 1.2. Hello world!
Get the [hello world example](https://github.com/pSpaces/Programming-with-Spaces/blob/master/hello.md) to compile and run.

### Exercise 1.3. Tutorial example
Get the example of the first chapter of the tutorial to compile and run:
* [example in Java](https://github.com/pSpaces/jSpace-examples/blob/master/tutorial/fridge-0/Fridge_0.java)

### Exercise 1.4. Playing with tuples
Play with the examples by trying the data types and operations seen in the tutorial.

### Exercise 1.5. More fun with tuples
Go to [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces) and play around with the online tool. 

## 2. Concurrent programming

### Exercise 2.1. Dining philosophers with a deadlock
The dining philosophers problem is a classical example of concurrent algorithms. A set of philosophers sit around a table. Each philosopher has a plate in front of him and there are forks in between each plate. A philosopher can grab the fork to the left of his plate and the one to the right of his plate. The lifecycle of a philosoper consist in continuously alternating thinking and lunch times. However, in order to eat they need to use two forks. Have a look at the dining philosophers code in this example:
* [example in Java](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_1/Exercise_2_1.java)

<!---
* [example in Go](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/dining-philosophers-0/main.go)
--->

and get the example to compile and run. If the example is not provided in your language of choice, implement it yourself.

### Exercise 2.2. Dining philosophers: driving them into a deadlock
Write a program in [tup4fun](http://www.formalmethods.dk/tup4fun/#tuple-spaces) that is similar to the wrong solution to the dining philosophers problem provided in Exercise 2.1. Use the simulation features of the tool to detect deadlocks (`stuck` configurations):
* Use the `Show configurations` feature by first setting reasonable limit on the `Number of Steps`.
* Use the `Show trace` feature by selecting a sufficiently `Number of Steps` until the trace ends up in a deadlock.
* Use the `Show trace` in combination with the choice button (the red crossed arrows) to force the scheduler to make the choices that lead to a deadlock. Try to get the shortest possible trace that leads to a deadlock.

**SOLUTIONS**
 * Tup4Fun: [solution1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/dining-philosophers-deadlock.txt)

### Exercise 2.3. Deadlock-free dining philosophers
The code provided in Exercise 2.1 is not a suitable solution to the dining philosophers problem since the philosophers can end up in a deadlock, where no philosopher is able to progress. Explain why and modify the example to avoid deadlocks. HINT: there are many solutions to this problem that you can find on the web. You may also use one of the coordination patterns seen in the tutorial.

**SOLUTIONS**:
 * Java: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_3/Exercise_2_2.java)

<!---
 * Go: [solution 1](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/dining-philosophers-1/main.go)
--->

### Exercise 2.4. Dining philosophers without deadlock
 Correct the program you wrote in Exercise 2.2 so that there is no deadlock. You can use your solution(s) to Exercise 2.3 as inspiration. You can try to use the simulation features to test the absence of deadlocks. For example, you can use `Show Resulting Configurations` and keep incrementing the `Number of Steps` until your are confident or the tool takes too much to provide an answer.

**SOLUTIONS**
 * Tup4Fun: [solution1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/dining-philosophers-solution-tickets.txt), [solution2](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/dining-philosophers-solution-asymmetry.txt)

### Exercise 2.5. Transition system for dining philosophers.
 A transition system is a graph where nodes are configurations of a program `P`, and there is an edge from a node/configuration `c` into a node/configuration `c'` if program `P` can perform one step from `c` into `c'`. Use a small instance (2 philosophers) of your solution to Exercise 2.2 and Exercise 2.4 to construct one transition system for each of the programs. Can you find deadlock (stuck) configurations in the transition system?

### Exercise 2.6. Parallel merge sort
A sorting algorithm takes a possibly disordered set of elements (e.g. a vector of integers) as input and returns the set of elements in order (e.g. as a vector of ordered integers). Sorting can be done in parallel, for example by parallelizing divide-and-conquer sorting algorithms like mergesort and quick-sort. Model and implement a parallel merge-sorting algorithm using tuple spaces. Use the producers/consumers coordination pattern where tasks are (ordered and unordered) vectors of integers and workers are specialised in various tasks: (i) splitting an unordered vector, (ii) merging two ordered vectors.

**SOLUTIONS**:
 * Java: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/java/Exercise_2_4/Exercise_2_4.java)

<!---
* Go: [solution 1](https://github.com/pSpaces/goSpace-examples/blob/master/tutorial/merge-sort-0/main.go)
--->

## 3. Distributed programming

### Exercise 3.1. Distributed dining philosophers
Consider your solution to exercise 2.3. Refactor your solution so that each tuple space is created and hosted by a different program and each philosopher runs as a standalone program. Try to run such distributed program in the same host and in different hosts.

**SOLUTIONS**:
 * Java [solution 1](https://gitlab.gbar.dtu.dk/02148/home/tree/master/solutions/java/Exercise_3_1)

<!---
* Go: [solution 1](https://github.com/pSpaces/goSpace-examples/tree/master/tutorial/dining-philosophers-2)
--->

### Exercise 3.2. Distributed merge sort
Consider your solution to exercise 2.6. Refactor your solution so that each tuple space is created and hosted by a different program and each worker runs as a standalone program. Try to run such distributed program in the same host and in different hosts.
 * Java [solution 1](https://gitlab.gbar.dtu.dk/02148/home/tree/master/solutions/java/Exercise_3_2)

<!---
* Go: [solution 1](https://github.com/pSpaces/goSpace-examples/tree/master/tutorial/merge-sort-1)
--->

### Exercise 3.3. A chat application
In [tutorial 03](https://github.com/pSpaces/Programming-with-Spaces/blob/master/tutorial-distributed-programming.md) we seen a simple chat application, where the server collects and print the chat messages. Implement a chat application where the chat messages are displayed on the clients as well.

## 4. Interaction-oriented programming

### Exercise 4.1
Benjamin has prepared the following protocol:

```
Alice.("forward", to) -> Bob.tupleName("forward", string);
if tupleName(to) == "Charlie"@Bob then
    Bob."hallo" -> Charlie.notUsed("hallo")
else
    Bob."hey" -> Dave._("hey")
```

Use [the `project` function](https://gitlab.gbar.dtu.dk/02148/home/blob/master/protocols.md#48-distributed-implementation-of-a-protocol) to project Benjamin's protocol.

**SOLUTIONS**:
- [Step-by-step solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_4_1.md)

### Exercise 4.2
Implement the [protocol example](https://gitlab.gbar.dtu.dk/02148/home/blob/master/protocols.md#49-a-complete-example) of chapter 4 using jSpaces. Hint: Use [the `project` function](https://gitlab.gbar.dtu.dk/02148/home/blob/master/protocols.md#48-distributed-implementation-of-a-protocol) described in the chapter to generate the distributed code.

## 5. Task-oriented programming

### Exercise 5.0
Consider the example of exercise 5.1 and try to model it in the [APO tool](https://apo.adrian-jagusch.de/). Generate the transition system (Analyze > Coverability Graph) and convince your self that you the Petri net behaves as expected.

### Exercise 5.1
Consider the following Petri net example describing a workflow with activities A, B, C and D:

![a Petri net](figures/Exercise7.1.png)

Use jSpace to implement the workflow where each activity of the workflow is implemented as an independent thread or program. Such threads/programs must interact through tuple spaces. You can try to model each place with a tuple space and each token with a tuple. What you need to do is to write code for each thread/program that behaves as the Petri net prescribes (i.e. consuming and producing the tokens from the right places). Running the concurrent/distributed program should behave like the Petri net: both should exhibit the same maximal activity traces (sequences of activities). You can do some testing by making each activity print its name so that you can check the order of execution and test them against the Petri net. You will have to consider some initial marking (for example, one where there is one token in the top-left place and a token in the top-right place). See slides for more details.

**SOLUTIONS**:
- [solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_5_1.md)

### Exercise 5.2
Sketch an approach to automatically generate concurrent or distributed code for a given Petri net. That is, generalise the solution that you implemented in Exercise 5.1.

**SOLUTIONS**:
- [solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_5_2.md)

### Exercise 5.3
Petri nets have been extended in different ways, for example to include *read* arcs, which connect places to transitions and impose the usual precondition requirements but do not consume tokens when firing transitions. The [Wikipedia](https://en.wikipedia.org/wiki/Petri_net) page also describes *reset* and *inhibitor* arcs. Extend your solution to Exercise 5.2 to consider those classes of arcs.

**SOLUTIONS**:
- [solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_5_3.md)

## 6. Stream oriented programming

### Exercise 6.1
Implement the [sensor dataflow example](https://gitlab.gbar.dtu.dk/02148/home/blob/master/streams.md#61-a-sensor-dataflow-example) sketched chapter 8. Use the [sketch to generate  code](https://gitlab.gbar.dtu.dk/02148/home/blob/master/streams.md#82-producers) described in the chapter. Make your own choices for the data that is being generated, and what the `prettify` function does.

<!---

## 4. Semantics of pSpace programs
In the following exercises consider the [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md) page and the transition systems obtained from such semantics: a transition system is a graph where nodes are programs and there is an edge from `M1 |- P1` to `M2 |- P2` if the semantics allows to conclude `M1 |-> P1 => M2 |-> P2`.

### Exercise 4.1. Dining philosophers, formally
Consider the following light pSpace program `S` which represents a simple version of the dining philosophers problem:

```
board |-> "fork"*"fork" |-
  board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); 0
‖ board.get("fork"); board.get("fork"); board.put("fork"); board.put("fork"); 0
```

Construct the transition system reachable from `S`, i.e. start with the state/node `S` and keep adding transitions and new states until no more states/transitions can be added. If there are deadlock states (programs that cannot evolve) identify them.

**SOLUTION**: [solution](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_3.1.md)

### Exercise 4.2. Deadlock-free dining philosophers, formally
Consider your solution to exercise 2.2. Update the program of exercise 4.1. to avoid deadlocks that are not final states (i.e. of the form `M |- 0`).

**SOLUTION**: [solution 1](https://gitlab.gbar.dtu.dk/02148/home/blob/master/solutions/formal/exercise_3.2.md)

### Exercise 4.3. Semantics other spaces kinds
The [formal semantics for a light version of pSpaces](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md) focuses on sequential spaces only (class `SequentialSpace` in jSpace). How would you change the [semantic rules for the core API](https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics-light.md#operational-semantics-of-the-core-api) to formalise each of the [space kinds](https://github.com/pSpaces/Programming-with-Spaces/blob/master/guide.md#space-api) supported by jSpace (`QueueSpace`, `StackSpace`, `PileSpace`, and `RandomSpace`).

Solution: see [full semantics] (https://github.com/pSpaces/Programming-with-Spaces/blob/master/semantics.md).

--->

<!---

## 5. Modelling and analysing pSpaces programs with Spin

### Exercise 5.1. Installing SPIN
Install [SPIN](http://www.spinroot.com) or use the G-Bar installation of SPIN (accessible via [thinlinc](http://gbar.dtu.dk/faq/43-thinlinc)).

### Exercise 5.2. Playing with SPIN
Try all examples under "Examples" in the [notes on how to use the Spin model checker to model and verify pSpace applications](https://github.com/pSpaces/Programming-with-Spaces/blob/master/Promela/promela.md) page.

### Exercise 5.3. Checking deadlock-freedom of dining philosophers
Consider your solution to exercise 2.2. Does it correspond to some of the examples of Exercise 5.2? If not, try to model your solution in Promela and check whether it is deadlock free.

--->

<!---

## 9. Secure spaces

### Exercise 9.1
Reflect on access control issues in your project:
* Is access control needed?
* What would be the advantages of applying access control?
* Would there be disadvantages? If yes, which?
* Which policies would you need?
* Can you write your policies in the simple access control policy language seen in class? If not, what are the limitations of the language and what else should the language support?

--->
