# 5. Task-oriented programming

Task-oriented programming is a top-down approach to coordination, where we see applications as sets of tasks to be executed according to some rules that regulate the order of execution. Those rules impose control flow dependencies among tasks.

## 5.1 Simple "happens-before" workflows

We start with a simple task-oriented programming model in which tasks can be executed only once and the only kind of control flow dependency is of the form "task A has to be executed before task B".

More formally, we define a workflow as a set (N,E) such that
* N is a set of tasks
* E is a set of pairs of tasks
* The graph obtained by taking N as a set of nodes and E as a set of edges is acyclic.

## 5.2 A simple workflow
Consider the following example:
* N = {Alice,Bob,Charlie,Dave}
* E = { (Alice,Bob), (Alice,Charlie), (Bob,Dave) , (Charlie,Dave) }

Which we can represent graphically as
```
      /---> Bob -------\
     |                 |
     |                 v
 Alice                Dave
     |                 ^
     |                 |
      \---> Charlie ---/
```

Informally, the workflow says that Alice does not need to wait for anyone, Bob and Charlie need to wait for Alice to be done, and Dave needs to wait for both Bob and Charlie. As a result, the application will execute Alice first, then Bob and Charlie in any order or concurrently, and finally Dave.

## 5.3 Implementing simple workflows
A workflow can be implemented as a pSpace application in several ways. We present first a simple approach to generate a *concurrent* implementation and will later discuss how to generate a *distributed* implementation.

The idea to generate a concurrent implementation for a workflow (N,E) is as follows:
1. Create a space `rules` and put all control flow dependencies (i.e. all pairs in E) into `rules`.
2. Introduce rules (X,Main) for each activity X no other activity is waiting for. This will be used to detect termination of the workflow.
3. Create an empty space `tokens`.
4. For each task X in N build and launch a process Coordinate(X) that proceeds as follows:
 * read all control flow dependencies (\_,X) and (X,\_) from `rules`
 * for each control flow dependency (Y,X), wait to see token `(Y,X)` as a tuple in space `tokens`
 * execute task X
 * for each control flow dependency (X,Y), put token `(X,Y)` as a tuple in space `tokens`
5. For each rule (X,Main), wait token `(X,Main)`.

## 5.4 Distributed implementation of simple workflows
This approach to implement a workflow as a concurrent program can be extended to generate distributed implementation as well. All we would need to decide is where to locate the rules, tokens and task coordinators. In the extreme case in which each task coordinator needs to be run as a standalone programs (possibly in different hosts), a reasonable approach for distributing tokens and rules would be to place them in spaces close to the programs. For instance, each token `(X,Y)` could be assigned to a space hosted by the coordinator of `Y` (as a sort of inbox).

## 5.5 Petri nets
The approach sketched above can also be extended to deal with richer forms of task-oriented specifications, for example workflows including standard control flow constructs such as choice choice points to choose among alternative continuations of the control flow or loops. A very general form of workflow is given by Petri nets.

A Petri net is a graph with two classes of nodes: *places* and *transitions*. For our purpose you can think of transitions as tasks, and places as containers of control flow tokens. The edges in a Petri net are called *flows* and go either from a place into a transition or vice-versa. An edge (p,t) from a place p to a transition t models the fact that the transition (task) needs a token in p to be executed (and possibly others). The place p is called a pre-place of t. An edge (t,p) from a transition to a place p models the fact that executing t generates a token in p. The place is called a post-place of p. The execution of a transition is atomic: simultaneously, one token from each pre-place is consumed and one token in each post-place is produced.

Formally, a Petri net is a tuple (P,T,F) where
* P is the set of places
* T is the set of transitions
* F is a multiset of pairs (p,t) where p is in P and t is in T

The state of a Petri net is called a *marking* and is defined by the number of tokens in each place. Given a marking m, the Petri net can perform one step by firing a transition t if m contains at least one token in each pre-place of t. The result of the step is another marking, where one token in each pre-place has been removed, and one token in each post-place of t has been added.

## 5.6 The simple workflow as a Petri nets
The above example of a simple workflow can be represented by a Petri net whose transitions are `{Alice, Bob, Charlie, Dave}` and where each edge `(x,y)` is represented by one place `(x,y)` and two flows: `x->(x,y)` and `(y,x)->y`.

The resulting Petri net is the following, where `p0` is the leftmost place:

```
p0 -> Alice
Alice -> (Alice,Bob)
Alice -> (Alice,Charlie)
(Alice,Bob) -> Bob
(Alice,Charlie) -> Charlie
Bob -> (Bob,Dave)
Charlie -> (Charlie,Dave)
(Bob,Dave) -> Dave
(Charlie,Dave) -> Dave
```
Graphically

![A petri net](figures/Example-petri.png)

Such Petri net can be easily modified to introduce variations in the example, like a choice that forces to choose between either `Bob` or `Charlie` (and `Dave` waiting for any of them). And a loop that allows the workflow to restart once `Dave` is done.

You can read more about Petri nets in the slides available on CampusNet.

## Summary

We have seen:
 * A simple approach to task-oriented programming based on "happens-before" constraints between tasks.
 * A brief introduction to Petri nets, a basic model to represent more general workflows.
